// Code generated by 'ccgo -export-defines  -export-enums  -export-externs X -export-fields F -export-structs  -export-typedefs  -lmodernc.org/x11/lib -o lib/xrender_linux_amd64.go -pkgname xrender -trace-translation-units /tmp/go-generate-2647540015/cdb.json libXrender.a', DO NOT EDIT.

package xrender

var CAPI = map[string]struct{}{
	"XRenderAddGlyphs":                {},
	"XRenderAddTraps":                 {},
	"XRenderChangePicture":            {},
	"XRenderComposite":                {},
	"XRenderCompositeDoublePoly":      {},
	"XRenderCompositeString16":        {},
	"XRenderCompositeString32":        {},
	"XRenderCompositeString8":         {},
	"XRenderCompositeText16":          {},
	"XRenderCompositeText32":          {},
	"XRenderCompositeText8":           {},
	"XRenderCompositeTrapezoids":      {},
	"XRenderCompositeTriFan":          {},
	"XRenderCompositeTriStrip":        {},
	"XRenderCompositeTriangles":       {},
	"XRenderCreateAnimCursor":         {},
	"XRenderCreateConicalGradient":    {},
	"XRenderCreateCursor":             {},
	"XRenderCreateGlyphSet":           {},
	"XRenderCreateLinearGradient":     {},
	"XRenderCreatePicture":            {},
	"XRenderCreateRadialGradient":     {},
	"XRenderCreateSolidFill":          {},
	"XRenderExtensionInfo":            {},
	"XRenderExtensionName":            {},
	"XRenderFillRectangle":            {},
	"XRenderFillRectangles":           {},
	"XRenderFindDisplay":              {},
	"XRenderFindFormat":               {},
	"XRenderFindStandardFormat":       {},
	"XRenderFindVisualFormat":         {},
	"XRenderFreeGlyphSet":             {},
	"XRenderFreeGlyphs":               {},
	"XRenderFreePicture":              {},
	"XRenderParseColor":               {},
	"XRenderQueryExtension":           {},
	"XRenderQueryFilters":             {},
	"XRenderQueryFormats":             {},
	"XRenderQueryPictIndexValues":     {},
	"XRenderQuerySubpixelOrder":       {},
	"XRenderQueryVersion":             {},
	"XRenderReferenceGlyphSet":        {},
	"XRenderSetPictureClipRectangles": {},
	"XRenderSetPictureClipRegion":     {},
	"XRenderSetPictureFilter":         {},
	"XRenderSetPictureTransform":      {},
	"XRenderSetSubpixelOrder":         {},
}
