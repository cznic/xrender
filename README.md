# xrender

CGo-free port of libXrender, the Render Extension to the X11 protocol

## Installation

    $ go get modernc.org/xrender

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/xrender/lib

## Documentation

[godoc.org/modernc.org/xrender](http://godoc.org/modernc.org/xrender)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxrender](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxrender)
