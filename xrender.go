//  Copyright 2021 The Xrender-Go Authors. All rights reserved.
//  Use of this source code is governed by a BSD-style
//  license that can be found in the LICENSE file.

//go:generate go run generator.go

package expat
